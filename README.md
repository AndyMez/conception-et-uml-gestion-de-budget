# Conception & UML partie 1 - Gestion de budget

## Description du projet

Afin de négocier une augmentation à votre travail, ou pour en trouver un nouveau, vous avez décidé de mettre à jour votre portfolio sur gitlab.

Vous avez décidé de coder un tout nouveau projet, qui mettra en valeur l'ensemble de vos compétences.

Pour joindre l'utile à l'agréable, et afin de mieux maîtriser vos dépenses mensuelles, vous avez décidé de vous créer une petite application de gestion de budget que vous allez utiliser dans le cadre de votre foyer.

## Notes 
### Use Case 

Pour le use case, j'ai créer un sujet qui aura toute la partie gestion de budget, qui me permettra de créer, modifier ou encore supprimer une entrée/sortie d'argent.
J'aurais aussi la possibilité de gelé une entrée afin que cette dernière ne soit plus modifiable ou lui modifier sa catégorie.

En tant que fonctionnalité supplémentaire j'aurais accès aux statistiques de mon budget, comme par exemple le pourcentage de dépense d'une catégorie particulier

### Diagramme de classe

Concernant le diagramme de classe, j'ai créer 4 tables : budget, inputs, outputs et statistiques, chacun possédant un id, un amount (montant d'argent), catégorie et une date. Mon budget a une relation one to many vers mes entrées et sorties d'argent.
Puis pour les méthodes, j'en ai créer afin d'ajouter un montant d'argent par défaut, un filtre par unité ou plusieurs, supprimé le montant ajouté par défaut. Concernant mes entrées et sorties et j'ai plus ou moins les même chose : ajouts d'entrées/sorties, édition d'entrées/sorties, delete entée/sorties